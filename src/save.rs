use clap::{Parser, Subcommand};

#[derive(Parser)]
#[command(about, version, author, long_about = None)]
struct Args {
    #[arg(short, long, default_value = "127.0.0.1:25575")]
    pub address: String,

    #[arg(short, long)]
    pub password: String,

    #[command(subcommand)]
    pub sub_command: SubCommand
}

#[derive(Subcommand)]
enum SubCommand {
    All,
    On,
    Off
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = Args::parse();

    let mut conn = rcon::Connection::builder()
        .enable_minecraft_quirks(true)
        .connect(args.address, &args.password).await
        .expect("Failed to establish connection to server");

    match args.sub_command {
        SubCommand::All => conn.cmd("save-all").await
            .expect("Failed to send `save-all` command"),
        SubCommand::On => conn.cmd("save-on").await
            .expect("Failed to send `save-on` command"),
        SubCommand::Off => conn.cmd("save-off").await
            .expect("Failed to send `save-off` command")
    };
}
