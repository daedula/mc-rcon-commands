use clap::Parser;

#[derive(Parser)]
#[command(about, version, author, long_about = None)]
struct Args {
    #[arg(short, long, default_value = "127.0.0.1:25575")]
    pub address: String,

    #[arg(short, long)]
    pub password: String
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = Args::parse();

    let mut conn = rcon::Connection::builder()
        .enable_minecraft_quirks(true)
        .connect(args.address, &args.password).await
        .expect("Failed to establish connection to server");

    if let Err(err) = conn.cmd("kick @a Server is going down!").await {
        match err {
            rcon::Error::Auth => panic!("Authentication failure! Did you provide the right password?"),
            rcon::Error::CommandTooLong => eprintln!("Kick command was too long; continuing anyways"),
            rcon::Error::Io(err) => panic!("IO error sending kick command: {:?}", err)
        }
    }

    conn.cmd("stop").await.expect("Failed to stop server");
}
