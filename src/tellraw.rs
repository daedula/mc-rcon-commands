use clap::Parser;

#[derive(Parser)]
#[command(about, version, author, long_about = None)]
struct Args {
    #[arg(short, long, default_value = "127.0.0.1:25575")]
    pub address: String,

    #[arg(short, long)]
    pub password: String,

    #[arg(short, long)]
    pub target: String,

    pub message: String
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = Args::parse();

    let mut conn = rcon::Connection::builder()
        .enable_minecraft_quirks(true)
        .connect(args.address, &args.password).await
        .expect("Failed to establish connection to server");

    conn.cmd(format!("say {} {}", args.target, args.message).as_str()).await
        .expect("Failed to send `tellraw` command");
}
